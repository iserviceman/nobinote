<sec-header>
    <div class="sec-topbar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-12 col-xl-12">
                        <ul class="list-inline text-center justify-content-center">
                            <li id="adds" class="list-inline-item" onclick="createNote()">Add
                                <div id="container-create-note" class="container-create-note active"></div>
                            </li>
                            <li class="list-inline-item">
                                <div id="container-se">
                                    <input type="text" class="form-control" placeholder="Search..." aria-label="Search...">
                                </div>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</sec-header>
