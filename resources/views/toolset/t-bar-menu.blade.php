<header>
    <div class="topbar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 col-md-5 col-xl-5">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <i id="l-bars" class="fas fa-bars fa-lg l-bars" onclick="toggleMainSidebar()"></i>
                        </li>
                        <li class="list-inline-item ml-2 head-logo">NOBINOTE</li>
                    </ul>

                </div>
                <div class="col-8 col-md-7 col-xl-7">
                    <div class="float-right">
                        <!-- Links -->
                        <ul class="navbar-nav">
                            <!-- Dropdown -->
                            <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" onclick="clearSpeedbars()">
                                  ตั้งค่า
                              </a>
                              <div class="dropdown-menu">
                                <ul class="dropbar-menu">
                                    <li><h6 class="dropdown-header">Setting header</h6></li>
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                </ul>
                                <ul class="dropbar-menu">
                                    <li><h6 class="dropdown-header">Setting header</h6></li>
                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                </ul>
                              </div>
                            </li>
                          </ul>
                      </div>
                      <span class="float-right mr-5 link-search">ค้นหา</span>
                </div>
            </div>
        </div>
    </div>
</header>
