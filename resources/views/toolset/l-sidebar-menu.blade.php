<l-sidebar id="55">
    <div class="wrapper">
        <div id="l-sidebar" class="l-sidebar active">
            <ul class="list-side-bars">
                <li>
                    <div class="flex-container-account-users">
                        <div class="wrapper-img-users">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <p class="fa-stack-1x img-users">D</p>
                            </span>
                        </div>
                        <div class="wrapper-account-users">
                            <p>Demo Name</p>
                            <p>demo@nobinote.com</p>
                        </div>
                    </div>
                </li>
            </ul>
            <hr class="section-break-l-sidebar-1" />
            <ul class="list-side-bars">
                <li>Note</li>
                <li>Announcement</li>
            </ul>
            <ul class="list-side-bars">
                <li><h6 class="">Tag</h6></li>
                <li>demo tag 1</li>
                <li>demo tag 2</li>
                <li>demo tag 3</li>
                <li>demo tag 4</li>
                <li>demo tag 5</li>
                <li>Edit Tag</li>
            </ul>
        </div>
    </div>
</l-sidebar>
