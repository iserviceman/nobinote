<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('link_styles.index')

    </head>
    <body>

        @include('toolset.l-sidebar-menu')
        @include('toolset.t-bar-menu')
        @include('toolset.sec-t-bar-menu')





        @yield('content')

        <script type="text/javascript">clearMainSidebar();</script>

    </body>
</html>
